#!/bin/bash

# message to user
echo "This script creates a keyfile to unlock the root system, so that the password doesn't need to be entered a second time."

KEYPART=/boot
KEYDIR=${TARGET}/${KEYPART}
KEYFILE=rootkey.bin

# Create key file:
dd if=/dev/urandom of=${KEYDIR}/${KEYFILE} bs=512 count=4

echo "Enter your LUKS partition password below to add filekey."
cryptsetup luksAddKey ${DEVICE}1 ${KEYDIR}/${KEYFILE} # This prompts for the LUKS container password

ln -sf /dev/mapper/${LUKSNAME} /dev

# Set crypttab:
echo "${LUKSNAME} ${CRYPTUUID} ${KEYPART}/${KEYFILE} luks" >> ${TARGET}/etc/crypttab

# message to user
echo "You have successfully created a LUKS keyfile and adjusted crypttab appropriately. You may proceed to the next step; execute the following in the terminal:"
echo ". 05-set-up-and-chroot.sh"
