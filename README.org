[[misc/void-on-zfs.png]]

This is a rough but working set of scripts to set up [[https://voidlinux.org/][Void Linux]] running on pure [[https://en.wikipedia.org/wiki/ZFS][ZFS]], =/=, =/home=, =/boot= and all on a fully [[https://gitlab.com/cryptsetup/cryptsetup][LUKS-encrypted]] partition.

* PSAs
** Better Workaround for Dracut [2019-11-30T09:38:11-07:00]
Rather than modifying =/etc/kernel.d/post-install/20-dracut=, create an appropriate Dracut configuration script. This is now integrated into the installation.

If you have a current install — assuming you did the default install (i.e. didn't change the location or name of the keyfile) — you can patch your curent installation by executing the following as root:
#+begin_src shell
#!/bin/bash

echo -e "hostonly=yes\ninstall_items+=\" /boot/rootkey.bin /etc/crypttab \"" >> /etc/dracut.d/10-crypt.conf
sed -i 's/\/rootkey.bin/\/boot\/rootkey.bin/' /etc/crypttab
#+end_src

* WARNING, WARNING
NOTICE: basically the first thing that these scripts do (in =01-cryptsetup.sh=) is to wipe the drive you're installing to. 

[[misc/danger.gif]]

So DON'T use this unless you're prepared to lose all data on the drive you're installing to. *The entire drive* (e.g. =/dev/sda=) will be wiped by =01-cryptsetup.sh=. 

The scripts are released under GPLv3, and come with absolutely no warranty, no refunds.

*  Basic Instructions
Run each script in order with =. <scriptname>= (in order to make variables available to next stage).

You need to somehow get the scripts over to the machine you're installing on. This may involve installing =git= and/or =rsync= on that machine.

For your installation medium, you might use either the [[https://github.com/leahneukirchen/hrmpf/][hrmpf rescue system]], itself built on Void Linux, or else an [[https://ubuntu.com/download/desktop][Ubuntu Live CD]] (the latter having the advantage of not having to recompile ZFS DKMS modules twice (one for the chroot-host, once in the chroot), and having an easier-to-get-working WiFi, so that is what I'd actually recommend).

* Caveats, Limitations
** ZFS pool dimensions
The scripts assume that you have a single drive that you're creating a single LUKS partition on for your ZFS pool which will be your Void Linux =/=, =/home=, =/boot= &c. 

If you have something more complex in mind, it shouldn't be too hard to modify the scripts accordingly, though LUKS encryption means you'll have to worry about multiple LUKS-encrypted device UUIDs and this information is made use of at several points in the scripts. Hopefully a future, more sophisticated version will have built in support for using mirrored, raidz, &c. groups of drives.

** BIOS/UEFI 
The scripts assume a 'BIOS', i.e. non-(U)EFI, set-up. This should be even easier to modify than the ZFS pool configuration, but I don't use UEFI on any of my current Void machines, so this is set up for a 'BIOS', Coreboot/Libreboot-compatible installation.

** libc, architecture
The scripts assume that the glibc flavour of Void will be installed. Installing musl libc instead should be relatively easy: just specify the relevant musl rootfs in =03-fetch-void-rootfs.sh= and comment out the locale setup in =06-config-inside-chroot.sh=. In theory it should work on ARM as well, though I don't think the ZFS DKMS modules currently build against the =rpi-kernel=.

** Post-installation configuration
The installation scripts have you set timezone, language(s), keyboard, system-name/hostname, root password, and a single non-root user account (& password). And there's some 'courtesy' stuff to make first-boot WiFi a bit easier (i.e. setting up NetworkManager), including a final first-boot script which (if the NetworkManager option is selected earlier in the installation) will enable the relevant services. But =sudo= access for the =wheel= group is not set up for you, nor is any graphical user interface set up. But, if you have internet access, the remainder of the post-installation configuration can be conducted as desired, perhaps following [[https://wiki.voidlinux.org/Post_Installation][the relevant bits of the Void Wiki]].
